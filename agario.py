import pygame
import math
import random
import time
from enum import Enum

# Define some colors
BLACK = (0, 0, 0)
WHITE = (255, 255, 255)
RED = (255, 0, 0)
GREEN = (0, 255, 0)
BLUE = (0, 0, 255)

WIDTH = 700
HEIGHT = 500

FONT = "serif"

GROW_RATE = 1
BLOCK_START_SIZE = 17
NUM_BLOCKS = 30

class State(Enum):
    MAIN_MENU = 1
    INSTRUCT = 2
    IN_GAME = 3
    LOST = 4
    WON = 5
    QUIT = 6

class Sounds():
    def __init__(self):
        self.start = pygame.mixer.Sound('assets/laser.ogg')
        self.grow = pygame.mixer.Sound('assets/laser.ogg')
        self.lost = pygame.mixer.Sound('assets/laser.ogg')
        self.won = pygame.mixer.Sound('assets/laser.ogg')

class Block(pygame.sprite.Sprite):
    def __init__(self, color, width, height):
        """ Constructor. Pass in the color of the block,
        and its x and y position."""
        super().__init__()

        self.image = pygame.Surface([width, height])
        self.image.fill(color)
        self.height = height
        self.width = width
        self.area = width*height

        # Fetch the rectangle object that has the dimensions of the image
        # Update the position of this object by setting the values
        # of rect.x and rect.y
        self.rect = self.image.get_rect()

class Coordinates():
    def __init__(self, x_start, y_start, x_end, y_end):
        self.x_start = x_start
        self.y_start = y_start
        self.x_end = x_end
        self.y_end = y_end

def setup():
    pygame.mixer.pre_init(44100, -16, 2, 512)
    pygame.mixer.init()
    
    pygame.init()
    pygame.display.set_caption("Agar.io ripoff")
    
    # Set the width and height of the screen [width, height]
    size = (WIDTH, HEIGHT)
    return pygame.display.set_mode(size)

def main_menu(screen):
    options = [
        "Start Game",
        "Instructions",
        "Quit",
    ]
    
    done = False
    index = 0

    while not done:
        for event in pygame.event.get():
            coords = print_menu_text(screen, options, index, BLACK)

            if event.type == pygame.QUIT:
                return State.QUIT

            elif event.type == pygame.KEYDOWN:
                if event.key == pygame.K_UP:
                    index = (index - 1) % 3

                elif event.key == pygame.K_DOWN:
                    index = (index + 1) % 3

                elif event.key == pygame.K_RETURN:
                    if index == 0:
                        return State.IN_GAME
                    elif index == 1:
                        return State.INSTRUCT
                    elif index == 2:
                        return State.QUIT

            elif event.type == pygame.MOUSEBUTTONDOWN:
                if mouse_inside(coords[0]):
                    return State.IN_GAME
                elif mouse_inside(coords[1]):
                    return State.INSTRUCT
                elif mouse_inside(coords[2]):
                    return State.QUIT

            elif event.type == pygame.MOUSEMOTION:
                if mouse_inside(coords[0]):
                    index = 0
                elif mouse_inside(coords[1]):
                    index = 1
                elif mouse_inside(coords[2]):
                    index = 2



def print_menu_text(screen, menu_options, index, bg_colour, heading=""):

    screen.fill(bg_colour)

    if heading:
        font = pygame.font.SysFont(FONT, 100)
        text = font.render(heading, True, WHITE)
        pos_x = (WIDTH/2) - (text.get_width()/2)
        pos_y = 75
        screen.blit(text, [pos_x, pos_y])

    font = pygame.font.SysFont(FONT, 50)
    spacing = 50
    
    vertical_centre = HEIGHT / 2
    middle = len(menu_options) / 2
    height = font.get_height()
    pos_y = vertical_centre - middle*(height + spacing) + spacing/2

    coords = []

    for i, option in enumerate(menu_options):
        if (i == index):
            text = font.render(option, True, RED)
        else:
            text = font.render(option, True, WHITE)
        pos_x = (WIDTH // 2) - (text.get_width() // 2)

        screen.blit(text, [pos_x, pos_y])
        coords.append(Coordinates(pos_x, pos_y, pos_x + text.get_width(), pos_y + height))
        pos_y = pos_y + text.get_height() + spacing

    pygame.display.flip()
    return coords

def generate_enemy_blocks(num_blocks, screen):

    pygame.mouse.set_pos([WIDTH//2, HEIGHT//2])

    block_list = pygame.sprite.Group() # List of 'enemy' sprites

    # TODO - make this range depend on the size of the screen
    for i in range(num_blocks):

        # This represents a block
        size = BLOCK_START_SIZE + num_blocks - i
        block = Block(BLACK, size, size)

        valid = False
        while not valid:
            # Set a random location for the block, 
            # subtract dimensions so block doesnt appear partly off screen
            x_pos = random.randrange(WIDTH - size)
            y_pos = random.randrange(HEIGHT - size)

            #prevent blocks from spawning too close to the player in the centre of the screen
            space = 15
            block.rect.x = x_pos
            block.rect.y = y_pos
            if not((x_pos + size > WIDTH//2 - space and x_pos < WIDTH//2 + space) and (y_pos + size > HEIGHT//2 - space and y_pos < HEIGHT//2 + space)) and not check_spawn_collisions(block, block_list):
                valid = True
        # Add the block to the list of objects
        block_list.add(block)

    return block_list
    
def check_spawn_collisions(new_block, existing_blocks):
    
    # See if the new block has collided with an existing block
    clashes = pygame.sprite.spritecollide(new_block, existing_blocks, False)
    return clashes

def check_game_collisions(player, block_list, all_sprites_list, sounds):
    
    # See if the player block has collided with anything.
    blocks_hit_list = pygame.sprite.spritecollide(player, block_list, False)

    # Check the list of collisions.
    for block in blocks_hit_list:
        if block.area <= player.area:

            # delete the block
            block_list.remove(block)
            all_sprites_list.remove(block)

            # check if that was the last block
            if not block_list:
                sounds.won.play()
                return State.WON

            else:
                """ Grow player size - done after we've drawn the 
                sprites since the player block defaults to position 0,0 """
                sounds.grow.play()
                oldPlayer = player
                all_sprites_list.remove(oldPlayer)
                player = Block(RED, oldPlayer.height + GROW_RATE, oldPlayer.width + GROW_RATE)
                all_sprites_list.add(player)                
        else:
            sounds.lost.play()
            return State.LOST

    return player

def play_game(screen, num_blocks):

    animation(screen, WHITE)

    # Loop until the user clicks the close button.
    done = False
    
    # Used to manage how fast the screen updates
    clock = pygame.time.Clock()

    # Hide the mouse cursor
    pygame.mouse.set_visible(False)

    # Initialise sounds
    sounds = Sounds()

    block_list = generate_enemy_blocks(num_blocks, screen)

    all_sprites_list = pygame.sprite.Group() # List of ALL sprites, used for drawing
    all_sprites_list.add(block_list)

    player = Block(RED, 20, 20)
    all_sprites_list.add(player)

    sounds.start.play()
    
    # -------- Main Program Loop -----------
    while not done:

        # --- Main event loop
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                return State.QUIT
    
        # --- Game logic should go here
        result = check_game_collisions(player, block_list, all_sprites_list, sounds)
        if result == State.WON or result == State.LOST:
            return result
        elif result is not None:
            player = result

        # --- Screen-clearing code goes here
        screen.fill(WHITE)
    
        # --- Drawing code should go here

        # Draw the player block so it centres on the mouse
        pos = pygame.mouse.get_pos()
        player.rect.x = pos[0] - player.width/2
        player.rect.y = pos[1] - player.height/2

        # Draw all the spites
        all_sprites_list.draw(screen)
            
        # --- Go ahead and update the screen with what we've drawn.
        pygame.display.flip()
    
        # --- Limit to 60 frames per second
        clock.tick(60)

def animation(screen, colour):
    for offset in range(0, HEIGHT+1, 5):
        pygame.draw.line(screen, colour, [0, offset], [WIDTH, offset], 5)
        pygame.display.flip()
        time.sleep(0.005)

def post_game(screen, result):
    message = ""
    animation(screen, BLACK)
    if result == State.WON:
        message = "Congratulations!"
    elif result == State.LOST:
        message = "Git gud"

    options = [
        "",
        "Retry",
        "Quit",
    ]

    done = False
    index = 1
    pygame.mouse.set_visible(True)

    while not done:
        for event in pygame.event.get():
            coords = print_menu_text(screen, options, index, BLACK, message)

            if event.type == pygame.QUIT:
                return State.QUIT
            elif event.type == pygame.KEYDOWN:
                if event.key == pygame.K_UP or event.key == pygame.K_DOWN:
                    if index == 1:
                       index =  2
                    elif index == 2:
                       index =  1

                elif event.key == pygame.K_RETURN:
                    if index == 1:
                        return State.IN_GAME
                    elif index == 2:
                        return State.QUIT

            elif event.type == pygame.MOUSEBUTTONDOWN:
                if mouse_inside(coords[0]):
                    return State.IN_GAME
                elif mouse_inside(coords[1]):
                    return State.INSTRUCT
                elif mouse_inside(coords[2]):
                    return State.QUIT

            elif event.type == pygame.MOUSEMOTION:
                if mouse_inside(coords[0]):
                    index = 0
                elif mouse_inside(coords[1]):
                    index = 1
                elif mouse_inside(coords[2]):
                    index = 2

def mouse_inside(coords):
    (x_pos, y_pos) = pygame.mouse.get_pos()
    return x_pos > coords.x_start and x_pos < coords.x_end and y_pos > coords.y_start and y_pos < coords.y_end

def main():
    screen = setup()

    done = False
    state = State.MAIN_MENU
    next_state = None

    # TODO - make these functions return a state enum
    while not done:
        if state == State.MAIN_MENU:
            next_state = main_menu(screen)

        # TODO - implement instructions screen
        elif state == State.INSTRUCT:
            next_state = main_menu(screen)

        elif state == State.IN_GAME:
            next_state = play_game(screen, NUM_BLOCKS)

        elif state == State.LOST:
            next_state = post_game(screen, State.LOST)

        elif state == State.WON:
            next_state = post_game(screen, State.WON)

        elif state == State.QUIT:
            pygame.quit()
            exit()
        state = next_state

# Call the main function, start up the game
if __name__ == "__main__":
    main()